/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datafstt_etl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author IdeaPad
 */
public class loader {
    
     public static int nb_cols_student = 2;
     public static int nb_cols_module = 4 ;
     public static int ingore_top_lines= 2 ;
     public static int header_ligne= 3;
     public static String prefix_module="";
    
    static void load(File file , int id_temps , int id_filiere){
        BufferedWriter writer=null;
        ArrayList<String> modules=new ArrayList<>();
        try {
            
       
            File selectedFile = file;
            if(selectedFile.exists()==false) {
                JOptionPane.showMessageDialog(null, "file n'exist pas ");
                return ;
            }
            System.out.println(file);
            BufferedReader sc = new BufferedReader(new FileReader(file));
            String name=file.getParent()+File.separator+"output"+File.separator+file.getName();
            File output=new File(name);
               if(output.exists()==true) output.delete();
               // File output_directory=new File();
                System.out.println(file.getParent()+File.separator+"output"+File.separator+file.getName());
                File p=  new File(file.getParent()+File.separator+"output"+File.separator);
                p.mkdirs();
                file.createNewFile();
            
             writer = new BufferedWriter(new FileWriter(output));
            writer.write("code_etud,cne,note,resultat,session,annee,code_module,code_filiere,id_temps\n");
            int i = 1;
                 String s=sc.readLine();
                while (s!=null) {

                   
                   
                    String[] cols= s.split(",");
                    int size=cols.length;
                    
                    if(i<=ingore_top_lines){
                        s = sc.readLine();    
                        i++;
                        continue;
                    }else if(i==header_ligne){
                        
                        for (int j = nb_cols_student; j < size; j+=nb_cols_module) {
                            String[] code= cols[j].split(":");
                            System.out.println(cols[j]);
                            String code_module="";
                            if(code.length>1)
                            code_module=code[1];
                            modules.add(prefix_module+code_module.trim());
                            
                            //System.out.println(code_module);
                        }
                        
                             s = sc.readLine();    
                            i++;
                            continue;
                    }
                    
                    StringBuilder sb=new StringBuilder();
                    
                    for (int j = 0; j < nb_cols_student; j++) {
                         sb.append(cols[j]);
                         sb.append(",");
                    }
                    
                     
                     int nb_module=(size-nb_cols_student) / nb_cols_student;
                     
                     int a=0;
                    for (int j = nb_cols_student; j < size; j+=nb_cols_module) {
                        StringBuilder line=new StringBuilder();
                        line.append(sb.toString());
                        String[] code= cols[j].split(":");
                       
                        for (int k = j; k < j+nb_cols_module; k++) {
                            line.append(cols[k]);
                            line.append(",");
                        }
                         
                         line.append(modules.get(a));
                         line.append(",");
                         line.append(id_filiere);
                         line.append(",");
                         line.append(id_temps);
                         
                         writer.write(line.toString()+"\n");
                         System.out.println(line);
                                 
                         a++;
                         
                    }
                    
                   
                   
                    
                    
                   i++;
                   s = sc.readLine();    

                }
               
                
                JOptionPane.showMessageDialog(null, "fin de la generation de votre fichier csv => "+name);
                

        } catch (Exception e) {
               e.printStackTrace();
        }finally{
            if(writer!=null){
                try {
                    writer.close();
                } catch (IOException ex) {
                    Logger.getLogger(loader.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    
}
}
